mod tools;
use tools::*;

#[test]
fn rustc_hello_world() {
    // Test stdout capture.
    let source = program!({
        pub fn main() {
            println!("Hello, world!");
        }
    });
    let (stdout, stderr) = tools::run_program(source, "");
    assert_eq!(stdout, "Hello, world!\n");
    assert_eq!(stderr, "");

    // Test stderr capture.
    let source = program!({
        pub fn main() {
            eprintln!("Hello, world!");
        }
    });
    let (stdout, stderr) = tools::run_program(source, "");
    assert_eq!(stdout, "");
    assert_eq!(stderr, "Hello, world!\n");

    // Test stdin injection.
    let source = program!({
        pub fn main() {
            std::io::copy(&mut std::io::stdin(), &mut std::io::stdout()).unwrap();
        }
    });
    let (stdout, stderr) = tools::run_program(source, "hello world");
    assert_eq!(stdout, "hello world");
    assert_eq!(stderr, "");
}

#[test]
#[should_panic]
fn program_panic() {
    let source = program!({
        pub fn main() {
            panic!("some panic");
        }
    });

    tools::run_program(source, "");
}

#[test]
fn use_spork() {
    let source = program!({
        #[allow(unused_imports)]
        use spork::*;

        pub fn main() {}
    });

    tools::run_program(source, "");
}
