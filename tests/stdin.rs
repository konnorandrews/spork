mod tools;

use tools::*;

#[test]
fn inherit() {
    let source = program!({
        use spork::*;

        fn print() {
            std::io::copy(&mut std::io::stdin(), &mut std::io::stdout()).unwrap();
        }

        pub fn main() {
            let mut table = DispatchTable::new();
            table.insert(print);
            spork(table);

            let (_, output) = Builder::new()
                .stdin(InputMode::Inherit)
                .spawn(print, ())
                .unwrap()
                .join_with_output()
                .unwrap();

            assert!(output.status.success());
            assert_eq!(output.stdout, []);
            assert_eq!(output.stderr, []);
        }
    });

    let (stdout, stderr) = run_program(source, "test input");

    assert_eq!(stdout, "test input");
    assert_eq!(stderr, "");
}

#[test]
fn disconnected() {
    let source = program!({
        use spork::*;

        fn print() {
            std::io::copy(&mut std::io::stdin(), &mut std::io::stdout()).unwrap();
        }

        pub fn main() {
            let mut table = DispatchTable::new();
            table.insert(print);
            spork(table);

            let (_, output) = Builder::new()
                .stdin(InputMode::Disconnected)
                .spawn(print, ())
                .unwrap()
                .join_with_output()
                .unwrap();

            assert!(output.status.success());
            assert_eq!(output.stdout, []);
            assert_eq!(output.stderr, []);
        }
    });

    let (stdout, stderr) = run_program(source, "test input");

    assert_eq!(stdout, "");
    assert_eq!(stderr, "");
}

#[test]
fn buffer() {
    let source = program!({
        use spork::*;

        fn print() {
            std::io::copy(&mut std::io::stdin(), &mut std::io::stdout()).unwrap();
        }

        pub fn main() {
            let mut table = DispatchTable::new();
            table.insert(print);
            spork(table);

            let (_, output) = Builder::new()
                .stdin(InputMode::Buffer(b"test input".to_vec()))
                .spawn(print, ())
                .unwrap()
                .join_with_output()
                .unwrap();

            assert!(output.status.success());
            assert_eq!(output.stdout, []);
            assert_eq!(output.stderr, []);
        }
    });

    let (stdout, stderr) = run_program(source, "test input");

    assert_eq!(stdout, "test input");
    assert_eq!(stderr, "");
}

#[test]
fn custom() {
    let source = program!({
        use spork::*;

        fn print() {
            std::io::copy(&mut std::io::stdin(), &mut std::io::stdout()).unwrap();
        }

        pub fn main() {
            let mut table = DispatchTable::new();
            table.insert(print);
            spork(table);

            let mut temp_path = std::env::current_exe()
                .unwrap()
                .parent()
                .unwrap()
                .to_path_buf();
            temp_path.push("output.txt");

            std::fs::write(&temp_path, "test input").unwrap();

            let file = std::fs::File::open(&temp_path).unwrap();

            let (_, output) = Builder::new()
                .stdin(InputMode::Custom(file.into()))
                .spawn(print, ())
                .unwrap()
                .join_with_output()
                .unwrap();

            assert!(output.status.success());
            assert_eq!(output.stdout, []);
            assert_eq!(output.stderr, []);
        }
    });

    let (stdout, stderr) = run_program(source, "");

    assert_eq!(stdout, "test input");
    assert_eq!(stderr, "");
}
