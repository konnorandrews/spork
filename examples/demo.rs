use spork::*;

fn f(name: String) {
    println!("hello {}", name);
}

fn add(a: f32, b: f32) -> f32 {
    // std::thread::sleep(std::time::Duration::from_secs_f32(10.0));
    a + b
}

fn main() {
    let mut table = DispatchTable::new();
    table.insert(f);
    table.insert(add);

    spork(table);

    // dbg!(spawn!(|| f("bob".into())).unwrap().join().unwrap());
    // dbg!(spawn!(|| add(1.0, 2.0)).unwrap().join_with_output().unwrap());
    dbg!(Builder::new()
        .spawn(add, (1.0, 2.0))
        .unwrap()
        .join()
        .unwrap());
}
