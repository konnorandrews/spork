use serde::{de::DeserializeOwned, Serialize};

use crate::errors::OpaqueError;

/// `Send` trait but for between processes.
///
/// In practice this trait is implemented for any type that can be serialized by `serde`.
///
/// The `'static` bound exists because values sent between processes can't hold references.
pub trait SendToProcess: Sized + 'static {
    /// Encode a value of the type for transfer to another process.
    fn encode(value: &Self) -> Result<Vec<u8>, OpaqueError>;

    /// Decode data created by a call to `encode` to a value of the type.
    fn decode(value: Vec<u8>) -> Result<Self, OpaqueError>;
}

impl<T> SendToProcess for T
where
    T: Serialize + DeserializeOwned + 'static,
{
    fn encode(value: &Self) -> Result<Vec<u8>, OpaqueError> {
        rmp_serde::to_vec(value).map_err(OpaqueError::new)
    }

    fn decode(value: Vec<u8>) -> Result<Self, OpaqueError> {
        rmp_serde::from_slice(&value).map_err(OpaqueError::new)
    }
}
