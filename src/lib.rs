// core modules
mod builder;
mod callable;
mod callable_id;
mod catch_panic;
mod data;
pub mod errors;
mod process_type;
mod send_process;

// blocking impl
mod blocking;

// async impl
// mod async;

// Public API.
pub use blocking::*;
pub use builder::*;
pub use callable::*;
pub use callable_id::*;
pub use process_type::*;
pub use send_process::*;
pub use catch_panic::going_to_catch_unwind;

// RUST_SPORK_ENV base64 encoded to reduce collision.
const ENV_VAR: &str = "UlVTVF9TUE9SS19FTlY";
