use crate::callable::CallableOnce;
use crate::callable_id::CallableHandle;
use crate::errors::SpawnError;
use std::collections::HashMap;
use std::ffi::{OsStr, OsString};
use std::path::{Path, PathBuf};

pub struct Builder<E> {
    pub(crate) environment: E,
    pub(crate) stdin: InputMode,
    pub(crate) stdout: OutputMode,
    pub(crate) stderr: OutputMode,
    pub(crate) args: Vec<OsString>,
    pub(crate) envs: HashMap<OsString, OsString>,
    pub(crate) cwd: Option<PathBuf>,
    pub(crate) kill_on_drop: bool,
}

// impl Builder<StdSpawnProcess> {
//     pub fn new() -> Self {
//         Self::new_with_spawn_environment(StdSpawnProcess)
//     }
// }

impl<E> Builder<E> {
    pub fn new_with_spawn_environment(environment: E) -> Self {
        let args = std::env::args_os().skip(1).collect();
        let envs = std::env::vars_os().collect();

        Builder {
            environment,
            stdin: InputMode::Inherit,
            stdout: OutputMode::Buffered {
                mode: Some(BufferMode::Line),
                device: Box::new(std::io::stdout()),
            },
            stderr: OutputMode::Buffered {
                mode: Some(BufferMode::Line),
                device: Box::new(std::io::stderr()),
            },
            args,
            envs,
            cwd: None,
            kill_on_drop: true,
        }
    }

    pub fn stdin(&mut self, cfg: InputMode) -> &mut Self {
        self.stdin = cfg;
        self
    }

    pub fn stdout(&mut self, cfg: OutputMode) -> &mut Self {
        self.stdout = cfg;
        self
    }

    pub fn stderr(&mut self, cfg: OutputMode) -> &mut Self {
        self.stderr = cfg;
        self
    }

    pub fn env<K, V>(&mut self, key: K, value: V) -> &mut Self
    where
        K: AsRef<OsStr>,
        V: AsRef<OsStr>,
    {
        self.envs.insert(key.as_ref().into(), value.as_ref().into());
        self
    }

    pub fn envs<I, K, V>(&mut self, vars: I) -> &mut Self
    where
        I: IntoIterator<Item = (K, V)>,
        K: AsRef<OsStr>,
        V: AsRef<OsStr>,
    {
        self.envs.extend(
            vars.into_iter()
                .map(|(key, value)| (key.as_ref().into(), value.as_ref().into())),
        );
        self
    }

    pub fn envs_remove<K>(&mut self, key: K) -> &mut Self
    where
        K: AsRef<OsStr>,
    {
        self.envs.remove(key.as_ref());
        self
    }

    pub fn env_clear(&mut self) -> &mut Self {
        self.envs.clear();
        self
    }

    pub fn get_envs(&self) -> impl Iterator<Item = (&OsStr, &OsStr)> {
        self.envs
            .iter()
            .map(|(key, value)| (key.as_os_str(), value.as_os_str()))
    }

    pub fn arg<T>(&mut self, arg: T) -> &mut Self
    where
        T: AsRef<OsStr>,
    {
        self.args.push(arg.as_ref().into());
        self
    }

    pub fn args<I, T>(&mut self, args: I) -> &mut Self
    where
        I: IntoIterator<Item = T>,
        T: AsRef<OsStr>,
    {
        self.args
            .extend(args.into_iter().map(|arg| arg.as_ref().into()));
        self
    }

    pub fn arg_clear(&mut self) -> &mut Self {
        self.args.clear();
        self
    }

    pub fn get_args(&self) -> impl Iterator<Item = &OsStr> {
        self.args.iter().map(|arg| arg.as_os_str())
    }

    pub fn current_dir<P>(&mut self, dir: P) -> &mut Self
    where
        P: AsRef<Path>,
    {
        self.cwd = Some(dir.as_ref().into());
        self
    }

    pub fn get_current_dir(&self) -> Option<&Path> {
        self.cwd.as_ref().map(|cwd| cwd.as_ref())
    }

    pub fn kill_on_drop(&mut self, do_kill: bool) -> &mut Self {
        self.kill_on_drop = do_kill;
        self
    }
}

pub trait SpawnProcess<A, R>: Sized {
    type Handle;

    fn spawn(
        builder: Builder<Self>,
        callable: CallableHandle<A, R>,
        args: A,
    ) -> Result<Self::Handle, SpawnError>;
}

#[derive(Clone, Debug, Copy)]
pub enum BufferMode {
    /// Each line is output in one write operation.
    ///
    /// For `stdin` this is the same as `Inherit`.
    Line,

    /// The output is only written after the child process exits.
    ///
    /// For `stdin` this is the same as `Inherit`.
    Full,

    /// The output is read in chunks of the given size.
    /// `LineBuffered` usually behaves better.
    ///
    /// For `stdin` this is the same as `Inherit`.
    Custom(usize),
}

pub enum OutputHandle {
    Stdio(std::process::Stdio),
}

pub enum OutputMode {
    /// The child inherits the same `Stdio` as the parent process.
    ///
    /// This can cause interlacing of text if
    /// both the parent and child are writing at the same time.
    Inherit,

    Buffered {
        mode: Option<BufferMode>,
        device: Box<dyn std::io::Write + Send>,
    },

    /// Provide a custom `Stdio` with a given buffer mode.
    ///
    /// When `mode` is some, then a thread will be spawned to manage writting in a buffered way to
    /// the `Stdio`.
    Custom(std::process::Stdio),

    /// The output is saved and will be available with `join_with_output`.
    Saved,

    /// the output is disconnected.
    ///
    /// This will result in any output being discarded.
    Disconnected,
}

impl OutputMode {
    pub fn take(&mut self, replacement: OutputMode) -> Self {
        match self {
            OutputMode::Inherit => OutputMode::Inherit,
            OutputMode::Saved => OutputMode::Saved,
            OutputMode::Disconnected => OutputMode::Disconnected,
            OutputMode::Buffered { .. } | OutputMode::Custom(_) => {
                std::mem::replace(self, replacement)
            }
        }
    }
}

pub enum InputMode {
    /// The child inherits the same `Stdio` as the parent process.
    ///
    /// The content of the `Stdio` will not be duplicated. So only either the parent
    /// or the child process will get it's content; not both.
    Inherit,

    /// Provide a custom `Stdio`.
    Custom(std::process::Stdio),

    /// Provide a buffer to write to the child process input.
    Buffer(Vec<u8>),

    /// the output is disconnected.
    ///
    /// This will result in any output being discarded.
    Disconnected,
}

impl InputMode {
    pub fn take(&mut self, replacement: InputMode) -> Self {
        match self {
            InputMode::Inherit => InputMode::Inherit,
            InputMode::Disconnected => InputMode::Disconnected,
            InputMode::Buffer(_) | InputMode::Custom(_) => std::mem::replace(self, replacement),
        }
    }
}
