use std::env::var_os;

use crate::ENV_VAR;

// Get the type of the current process.
pub fn current_process_type() -> ProcessType {
    // We use the environment variable to figure out if this is a child process
    // because it's the only consistent difference. Someone could set the exact
    // environment variable used, but that is not within the scope of this function.
    match var_os(ENV_VAR) {
        Some(_) => ProcessType::Child,
        None => ProcessType::Main,
    }
}

// Type of the process.
pub enum ProcessType {
    // The first process that spawned everything else.
    //
    // This is the process the user/system started.
    Main,

    // A child of the `Main` process or another `Child` process.
    //
    // This means that a `DispatchTable` should be active in this process.
    Child,
}
