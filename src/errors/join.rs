use super::{child::ChildError, opaque::OpaqueError};

/// Error while joining a child process.
#[derive(thiserror::Error, PartialEq, Debug)]
pub enum JoinError {
    /// A general IO error.
    #[error("{0}")]
    Io(OpaqueError),

    /// Failed to wait for the child process to exit.
    ///
    /// This is usually an error from the OS.
    #[error("error waiting for child process: {0}")]
    WaitFailed(OpaqueError),

    /// The child process did not return any data or specific error.
    ///
    /// This is usually an indication that the child process stopped unexpectedly.
    #[error("error receiving return value: {0}")]
    ReturnMissing(OpaqueError),

    /// Error while decoding the return of the callable.
    #[error("error while decoding return: {0}")]
    ReturnDecode(OpaqueError),

    /// The child process itself reported an error.
    #[error("{0}")]
    Child(#[from] ChildError),
}
