use serde::{Deserialize, Serialize};

use super::{dispatch::DispatchError, opaque::OpaqueError};

/// Error while running callable on the child process.
#[derive(thiserror::Error, Deserialize, Serialize, PartialEq, Debug)]
pub enum ChildError {
    /// The child process panicked.
    #[error("{0}")]
    Panic(ChildPanicInfo),

    /// Error from `DispatchTable`.
    #[error("{0}")]
    Dispatch(DispatchError),

    /// Error while deserializing the arguments to the callable.
    #[error("error while deserializing arguments: {0}")]
    ArgsDecode(OpaqueError),

    /// Error while serializing the return of the callable.
    #[error("error while serializing return: {0}")]
    ReturnEncode(OpaqueError),
}

#[derive(Debug, Deserialize, Serialize, PartialEq)]
pub struct ChildPanicInfo {
    message: String,
    file: Option<String>,
    line: Option<u32>,
    column: Option<u32>,
}

impl ChildPanicInfo {
    pub fn new(
        message: String,
        file: Option<String>,
        line: Option<u32>,
        column: Option<u32>,
    ) -> Self {
        Self {
            message,
            file,
            line,
            column,
        }
    }

    pub fn message(&self) -> &str {
        &self.message
    }

    pub fn file(&self) -> Option<&str> {
        self.file.as_deref()
    }

    pub fn line(&self) -> Option<u32> {
        self.line
    }

    pub fn column(&self) -> Option<u32> {
        self.column
    }
}

impl std::fmt::Display for ChildPanicInfo {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match (&self.file, &self.line, &self.column) {
            (Some(file), Some(line), Some(column)) => write!(
                f,
                "child process panicked at '{msg}', {file}:{line}:{column}",
                msg = self.message,
            ),
            (Some(file), Some(line), _) => write!(
                f,
                "child process panicked at '{msg}', {file}:{line}",
                msg = self.message,
            ),
            (Some(file), _, _) => write!(
                f,
                "child process panicked at '{msg}', {file}",
                msg = self.message,
            ),
            (_, _, _) => write!(f, "child process panicked at '{msg}'", msg = self.message,),
        }
    }
}

#[cfg(test)]
mod test {
    use super::ChildPanicInfo;

    #[test]
    fn panic_display() {
        let panic = ChildPanicInfo::new("some message".into(), Some("my_file".into()), Some(2), Some(3));

        assert_eq!(panic.to_string(), "child process panicked at 'some message', my_file:2:3")
    }
}
