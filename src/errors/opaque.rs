use serde::{Deserialize, Serialize};

/// An opaque error message.
///
/// Only the error message is available for debugging purposes.
#[derive(Deserialize, Serialize, Clone, PartialEq)]
#[serde(transparent)]
pub struct OpaqueError {
    msg: String,
}

impl OpaqueError {
    /// Create from anything that can be displayed.
    pub fn new<T>(error: T) -> Self
    where
        T: std::fmt::Display,
    {
        Self {
            msg: error.to_string(),
        }
    }
}

impl std::fmt::Display for OpaqueError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        <String as std::fmt::Display>::fmt(&self.msg, f)
    }
}

impl std::fmt::Debug for OpaqueError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        <String as std::fmt::Debug>::fmt(&self.msg, f)
    }
}
