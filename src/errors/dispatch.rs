use serde::{Deserialize, Serialize};

use super::opaque::OpaqueError;

#[derive(thiserror::Error, Serialize, Deserialize, Clone, PartialEq, Debug)]
pub enum DispatchError {
    /// A general IO error.
    #[error("{0}")]
    Io(OpaqueError),

    #[error("callable is not in the dispatch table")]
    CallableNotInDispatchTable,

    #[error("sent panic to parent process, but also got a result from the callable")]
    CaughtPanicButGotResult,
}
