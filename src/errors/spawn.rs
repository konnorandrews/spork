use super::opaque::OpaqueError;

/// Error spawning child process.
#[derive(thiserror::Error, PartialEq, Debug)]
pub enum SpawnError {
    /// A general IO error.
    #[error("{0}")]
    Io(OpaqueError),

    /// The parent process was unable to send the callable arguments to the child process.
    ///
    /// This indicates that the child process crashed early, or that the `DispatchTable` isn't
    /// setup correctly.
    #[error("failed to send arguments to child process: {0}")]
    FailedToSendArgs(OpaqueError),

    /// A child process can't be spawned if the `spork`/`try_spork` method on `DispatchTable` was
    /// never called.
    ///
    /// This indicates that a `DispatchTable` was never created.
    ///
    /// This check exists to stop the creation of "infinite" children processes.
    #[error("the `spork` function was never called")]
    SporkNeverCalled,

    /// The call to `spork`/`try_spork` directly before the spawn attempt failed.
    ///
    /// This indicates that a `DispatchTable` was incorrectly configured.
    ///
    /// This check exists to stop the creation of "infinite" children processes.
    #[error("the `spork` function failed previously")]
    SporkFailed,

    /// Error while serializing the arguments for the callable.
    #[error("error while serializing arguments: {0}")]
    ArgsEncode(OpaqueError),
}
