use ipc_channel::ipc::IpcBytesSender;
use std::any::Any;
use std::panic::{AssertUnwindSafe, PanicInfo};
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::{Arc, Mutex};

use crate::errors::DispatchError;
use crate::errors::{ChildError, ChildPanicInfo};

static IGNORE_PANIC: AtomicBool = AtomicBool::new(false);

pub fn going_to_catch_unwind<F, R>(func: F) -> R
where
    F: FnOnce() -> R,
{
    IGNORE_PANIC.store(true, Ordering::Relaxed);
    let result = func();
    IGNORE_PANIC.store(false, Ordering::Relaxed);
    result
}

pub fn catch_panic<F, R>(
    shared_sender: Arc<Mutex<IpcBytesSender>>,
    func: F,
) -> Result<R, DispatchError>
where
    F: FnOnce() -> R,
{
    // Get current thread id so we can not catch other thread's panics.
    let main_thread_id = std::thread::current().id();

    // Take the old panic hook so we can set it back later.
    let old_panic_hook: Arc<dyn Fn(&PanicInfo<'_>) + Sync + Send + 'static> =
        std::panic::take_hook().into();

    let handled_panic = Arc::new(AtomicBool::new(false));

    // There is a small amount of time here that a panic could happen on another thread without the
    // hook being set.

    // Set the panic hook so that as soon as a panic happens we can notify the parent process using
    // the IPC sender.
    std::panic::set_hook({
        let shared_sender = Arc::clone(&shared_sender);
        let old_panic_hook = Arc::clone(&old_panic_hook);
        let handled_panic = Arc::clone(&handled_panic);
        Box::new(move |panic_info| {
            // Don't catch panics of other threads.
            // Use the old panic hook to handle them incase the user set it.
            if std::thread::current().id() != main_thread_id {
                return old_panic_hook(panic_info);
            }

            // The `going_to_catch_unwind` function is active.
            // Use the default panic hook.
            if IGNORE_PANIC.load(Ordering::Relaxed) {
                return old_panic_hook(panic_info);
            }

            // Flag that we handled a panic on the entry point's thread.
            // If the flag was already set this is another panic.
            // We can't send more than one panic to the parent process as it's not expecting it.
            // So we fall back to the old panic hook to handle it.
            if handled_panic.swap(true, Ordering::Relaxed) {
                return old_panic_hook(panic_info);
            }

            // If the lock operation results in a Err then we don't want to panic.
            // Because if we did that would cause a double panic.
            // Instead we will just fail to send the panic info to the parent process.
            if let Ok(sender) = shared_sender.lock() {
                // Send the panic info to the parent process.
                send_panic_info_to_parent(panic_info, &sender);
            }

            // Continue with handling the panic. Either abort or unwind.
            // Unwind specific handling happens below.
        })
    });

    let result = std::panic::catch_unwind(AssertUnwindSafe(func));

    // This function should only be called once. So wrapping the panic handler shouldn't cause any
    // issues.
    std::panic::set_hook(Box::new(move |panic_info| old_panic_hook(panic_info)));

    match (result, handled_panic.load(Ordering::Relaxed)) {
        // No panic was handled by the above code, and no panic was caught by catch_unwind.
        (Ok(result), false) => Ok(result),

        // The entry point function caught the panic before us.
        (Ok(_), true) => Err(DispatchError::CaughtPanicButGotResult),

        // Panic was handled by the above code, and we caught the panic.
        // Resume the unwind into the calling code for cleanup.
        // There is a possibility that this is a different panic than the one handled above,
        // but if that's the case then the user caught then caused another panic.
        // We can't really do anything about that.
        (Err(payload), true) => std::panic::resume_unwind(payload),

        // A panic happened but the above code didn't handle it.
        // This happens if the entry point function or another thread sets the panic handler.
        // We need to now handle it here instead.
        (Err(payload), false) => {
            // If the lock operation results in a Err then we don't want to panic.
            // Because if we are want to resume the active panic.
            // Instead we will just fail to send the panic info to the parent process.
            if let Ok(sender) = shared_sender.lock() {
                // Send the panic payload to the parent process.
                send_panic_payload_to_parent(&payload, &sender);
            }

            std::panic::resume_unwind(payload)
        }
    }
}

fn send_panic_info_to_parent(panic_info: &PanicInfo<'_>, shared_sender: &IpcBytesSender) {
    // Get the payload of the panic as a string.
    // This follows the std default panic hook's way of getting the message.
    let msg = match panic_info.payload().downcast_ref::<&str>() {
        Some(s) => *s,
        None => match panic_info.payload().downcast_ref::<String>() {
            Some(s) => &s[..],
            None => "Box<dyn Any>",
        },
    };

    // Create panic info struct to send to parent.
    let result: Result<Vec<u8>, _> = Err(ChildError::Panic(ChildPanicInfo::new(
        msg.to_owned(),
        panic_info
            .location()
            .map(|location| location.file().to_owned()),
        panic_info
            .location()
            .map(|location| location.line().to_owned()),
        panic_info
            .location()
            .map(|location| location.column().to_owned()),
    )));

    // Send panic info to the parent process.
    // The serialization should never error.
    // The send result is ignored to stop this from maybe double panicking.
    // If this is an Err then the parent process will not get any result, but should also error.
    if let Ok(data) = rmp_serde::to_vec(&result) {
        let _ = shared_sender.send(&data);
    }
}

fn send_panic_payload_to_parent(
    panic_payload: &Box<dyn Any + Send + 'static>,
    shared_sender: &IpcBytesSender,
) {
    // Get the payload of the panic as a string.
    // This follows the std default panic hook's way of getting the message.
    let msg = match panic_payload.downcast_ref::<&str>() {
        Some(s) => *s,
        None => match panic_payload.downcast_ref::<String>() {
            Some(s) => &s[..],
            None => "Box<dyn Any>",
        },
    };

    // Create panic info struct to send to parent.
    // Sadly, we don't have any information about the location of the panic.
    let result: Result<Vec<u8>, _> = Err(ChildError::Panic(ChildPanicInfo::new(
        msg.to_owned(),
        None,
        None,
        None,
    )));

    // Send panic info to the parent process.
    // The serialization should never error.
    // The send result is ignored to stop this from maybe double panicking.
    // If this is an Err then the parent process will not get any result, but should also error.
    if let Ok(data) = rmp_serde::to_vec(&result) {
        let _ = shared_sender.send(&data);
    }
}
