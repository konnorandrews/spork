use ipc_channel::ipc::{IpcBytesReceiver, IpcBytesSender};
use serde::{Deserialize, Serialize};

/// Data sent back to the parent process.
#[derive(Serialize, Deserialize, Debug)]
pub(crate) struct ParentData {
    /// Channel to send to the child process.
    pub sender: IpcBytesSender,

    /// Channel to receive from the child process.
    pub receiver: IpcBytesReceiver,
}
