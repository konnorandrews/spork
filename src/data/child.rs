use std::path::PathBuf;

use serde::{Deserialize, Serialize};

use crate::callable_id::EncodedCallableID;

/// Data sent to the child process.
#[derive(Serialize, Deserialize, Debug)]
pub(crate) struct ChildData {
    /// Name of the parent's IPC channel.
    pub server_name: String,

    /// The entry to run.
    pub entry_id: EncodedCallableID,

    /// The file path chosen by the parent to signal the child is about to connect.
    pub signal_file_path: PathBuf,
}
