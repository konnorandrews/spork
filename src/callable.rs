/// Marker trait for types implementing `FnOnce`.
///
/// This trait is automatically implemented for functions and closures.
///
/// This trait exists because the special syntax for the `FnOnce` trait (aka `FnOnce(x, y, z) ->
/// w`) makes it difficault to do the generic bounds needed by this crate.
pub trait CallableOnce<A, R> {
    /// Call function.
    fn call(this: Self, args: A) -> R;
}

/// Reverse the order of a sequence of ident tokens.
macro_rules! rev_args {
    ($($swapped:ident)* |) => {
        impl_with_args!($($swapped)*);
    };
    ($($swapped:ident)* | $first_arg:ident $($arg:ident)*) => {
        rev_args!($first_arg $($swapped)* | $($arg)*);
    };
}

/// Impl `CallableOnce` given set of arguments.
macro_rules! impl_with_args {
    () => {
        impl<T, R> CallableOnce<(), R> for T
        where
            T: FnOnce() -> R
        {
            fn call(this: Self, _args: ()) -> R {
                this()
            }
        }
    };
    ($($arg:ident)*) => {
        impl<T, R, $($arg),*> CallableOnce<($($arg),*,), R> for T
        where
            T: FnOnce($($arg),*) -> R
        {
            fn call(this: Self, args: ($($arg),*,)) -> R {
                #[allow(non_snake_case)]
                let ($($arg),*,) = args;
                this($($arg),*)
            }
        }
    };
}

/// Recursively impl `CallableOnce` for all param amounts.
macro_rules! impl_with_args_all {
    () => {
        impl_with_args!();
    };
    ($last_arg:ident $($arg:ident)*) => {
        impl_with_args_all!($($arg)*);
        rev_args!(| $last_arg $($arg)*);
    };
}

// Implement for everything with 30 params or less.
impl_with_args_all!(A30 A29 A28 A27 A26 A25 A24 A23 A22 A21 A20 A19 A18 A17 A16 A15 A14 A13 A12 A11 A10 A9 A8 A7 A6 A5 A4 A3 A2 A1);

#[test]
fn call_0_arg() {
    let mut x = false;
    let test = || x = true;
    let _: () = CallableOnce::call(test, ());
    assert!(x);
}

#[test]
fn call_1_arg() {
    let mut x = false;
    let test = |_: u32| {
        x = true;
        12.3
    };
    let _: f32 = CallableOnce::call(test, (42u32,));
    assert!(x);
}

#[test]
fn call_2_arg() {
    let mut x = false;
    let test = |_: u32, _: i32| {
        x = true;
        false
    };
    let _: bool = CallableOnce::call(test, (42u32, 123i32));
    assert!(x);
}

#[test]
fn call_3_arg() {
    let mut x = false;
    let test = |_: u32, _: i32, _: u32| {
        x = true;
        false
    };
    let _: bool = CallableOnce::call(test, (42u32, 123i32, 123u32));
    assert!(x);
}
