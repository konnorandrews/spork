mod dispatch_table;
mod join_handle;
mod macros;
mod spawn;

use std::sync::{atomic::Ordering, Arc, Mutex};

pub use dispatch_table::*;
use ipc_channel::ipc::{bytes_channel, IpcBytesReceiver, IpcBytesSender, IpcSender};

use std::sync::atomic::AtomicBool;

use crate::{
    catch_panic::catch_panic,
    data::{child::ChildData, parent::ParentData},
    errors::{ChildError, DispatchError, OpaqueError},
    process_type::{current_process_type, ProcessType},
};

pub(crate) static SPORK_METHOD_CALLED: AtomicBool = AtomicBool::new(false);
pub(crate) static SPORK_FAILED: AtomicBool = AtomicBool::new(true);

#[track_caller]
pub fn spork(table: DispatchTable) {
    if let Err(err) = try_spork(table) {
        if let ProcessType::Main = current_process_type() {
            panic!("{}", err);
        }

        // The only error we can reliably report is callable not in dispatch table.
        if let DispatchError::CallableNotInDispatchTable = err {
            let data = std::env::var(crate::ENV_VAR).unwrap();
            let data = base64::decode(data).expect("Failed to decode child data.");
            let data: ChildData =
                rmp_serde::from_slice(&data).expect("Failed to deserialize child data.");

            let result = || -> Result<(), DispatchError> {
                let (sender, receiver, _server_sender) = connect_to_parent(&data)?;

                let _ = receiver.recv();

                let result: Result<Vec<u8>, _> = Err(ChildError::Dispatch(err.clone()));

                if let Ok(data) = rmp_serde::to_vec(&result) {
                    let _ = sender
                        .send(&data)
                        .map_err(|err| DispatchError::Io(OpaqueError::new(err)))?;
                }

                Ok(())
            }();

            if let Err(_) = result {
                panic!("{}", err);
            }
        } else {
            // Show the error.
            panic!("{}", err);
        }

        std::process::exit(-1);
    }
}

pub fn try_spork(mut table: DispatchTable) -> Result<(), DispatchError> {
    SPORK_METHOD_CALLED.store(true, Ordering::Relaxed);
    match try_spork_impl(&mut table) {
        Ok(result) => Ok(result),
        Err(err) => {
            SPORK_FAILED.store(true, Ordering::Relaxed);
            Err(err)
        }
    }
}

fn try_spork_impl(dispatch_table: &mut DispatchTable) -> Result<(), DispatchError> {
    if let ProcessType::Main = current_process_type() {
        SPORK_FAILED.store(false, Ordering::Relaxed);

        return Ok(());
    }

    // Unwrap is used because is_child already checked for it.
    // If it's not utf-8 then something bad happend.
    let data = std::env::var(crate::ENV_VAR).unwrap();
    let data = base64::decode(data).expect("Failed to decode child data.");
    let data: ChildData = rmp_serde::from_slice(&data).expect("Failed to deserialize child data.");

    let entry_point = dispatch_table
        .table
        .remove(&data.entry_id)
        .ok_or_else(|| DispatchError::CallableNotInDispatchTable)?;

    let (sender, receiver, server_sender) = connect_to_parent(&data)?;

    let args = receiver.recv().unwrap();

    let shared_sender = Arc::new(Mutex::new(sender));

    SPORK_FAILED.store(false, Ordering::Relaxed);

    let result = catch_panic(shared_sender.clone(), move || entry_point.run(args))?;

    shared_sender
        .lock()
        .unwrap()
        .send(&rmp_serde::to_vec(&result).expect("Failed to serialize child result."))
        .expect("Failed to send result to parent.");

    // Make sure that the IPC channels are cleaned up.
    // ipc_channel can handle if they aren't, but its good to anyways.
    drop(shared_sender);
    drop(server_sender);

    std::process::exit(0);
}

fn connect_to_parent(
    data: &ChildData,
) -> Result<(IpcBytesSender, IpcBytesReceiver, IpcSender<ParentData>), DispatchError> {
    // Create signal file just before connecting to signal we are about to connect.
    std::fs::write(&data.signal_file_path, "").unwrap();

    let server_sender = IpcSender::connect(data.server_name.to_owned())
        .expect("Failed to connect to parent process.");

    let (parent_sender, receiver) =
        bytes_channel().expect("Failed to create bidirectional channel.");

    let (sender, parent_receiver) =
        bytes_channel().expect("Failed to create bidirectional channel.");

    let parent_data = ParentData {
        sender: parent_sender,
        receiver: parent_receiver,
    };

    server_sender
        .send(parent_data)
        .expect("Failed to send channels to parent.");

    Ok((sender, receiver, server_sender))
}
