#[macro_export]
macro_rules! spawn {
    (|| $function:ident()) => {
        $crate::Builder::new().spawn($function, ())
    };
    (|| $function:ident($($arg:tt)*)) => {
        $crate::Builder::new().spawn($function, ($($arg)*,))
    };
    (|| ref $function:ident()) => {
        $crate::Builder::new().spawn_handle($function, ())
    };
    (|| ref $function:ident($($arg:tt)*)) => {
        $crate::Builder::new().spawn_handle($function, ($($arg)*,))
    };
    (|| $($token:tt)*) => {
        compile_error!(concat!("Expected function call or `ref` before a function call. Cannot spawn closure with arbitrary expression `", stringify!($($token)*), "`."))
    };
    ($($token:tt)*) => {
        compile_error!(concat!("Expected closure syntax. Help: try `spawn!(|| ", stringify!($($token)*), ")`"))
    };
}
