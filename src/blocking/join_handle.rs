use std::{
    io::Read,
    process::{Child, ExitStatus, Output},
};

use ipc_channel::ipc::{IpcBytesReceiver, IpcError};

use crate::errors::{ChildError, JoinError, OpaqueError};

type DecodeReturn<T> = fn(Vec<u8>) -> Result<T, OpaqueError>;

pub struct JoinHandle<T> {
    // Handle to child process.
    pub(super) handle: Child,

    // Function to decode the return from the process.
    pub(super) decode_return: DecodeReturn<T>,

    // IPC channel to get result from child process.
    pub(super) return_receiver: IpcBytesReceiver,

    // Handle to thread reading in the child stdout.
    pub(super) stdout_thread_handle: Option<std::thread::JoinHandle<()>>,

    // Handle to thread reading in the child stderr.
    pub(super) stderr_thread_handle: Option<std::thread::JoinHandle<()>>,

    pub(super) kill_on_drop: bool,
}

impl<T> Drop for JoinHandle<T> {
    fn drop(&mut self) {
        if self.kill_on_drop {
            // We did our best, just ignore any errors.
            let _ = self.handle.kill();
        }
    }
}

impl<T> std::fmt::Debug for JoinHandle<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("JoinHandle")
            .field("process_id", &self.handle.id())
            .field("kill_on_drop", &self.kill_on_drop)
            .finish()
    }
}

impl<T> JoinHandle<T> {
    /// Checks if the associated process has finished running.
    pub fn is_finished(&mut self) -> Result<bool, std::io::Error> {
        Ok(self.handle.try_wait()?.is_some())
    }

    pub fn join(mut self) -> Result<T, JoinError> {
        let (result, _status) =
            get_return_value(&mut self.handle, &self.return_receiver, self.decode_return)?;

        Ok(result)
    }

    pub fn join_with_output(mut self) -> Result<(T, Output), JoinError> {
        let (result, status) =
            get_return_value(&mut self.handle, &self.return_receiver, self.decode_return)?;

        let (stdout, stderr) = read_output(&mut self.handle)?;

        Ok((
            result,
            std::process::Output {
                stdout,
                stderr,
                status,
            },
        ))
    }

    pub fn child(&self) -> &Child {
        &self.handle
    }

    pub fn child_mut(&mut self) -> &mut Child {
        &mut self.handle
    }

    pub fn kill(mut self) -> Result<Output, JoinError> {
        // Kill the process.
        self.handle
            .kill()
            .map_err(|err| JoinError::Io(OpaqueError::new(err)))?;

        // Read any output it generated.
        let (stdout, stderr) = read_output(&mut self.handle)?;

        // Try to get the exit status.
        let status = self
            .handle
            .try_wait()
            .map_err(|err| JoinError::Io(OpaqueError::new(err)))?
            .ok_or_else(|| JoinError::Io(OpaqueError::new("failed to get exit status.")))?;

        Ok(Output {
            status,
            stdout,
            stderr,
        })
    }
}

fn read_output(handle: &mut Child) -> Result<(Vec<u8>, Vec<u8>), JoinError> {
    let stdout = handle
        .stdout
        .as_mut()
        .map(|stdout| -> std::io::Result<_> {
            let mut buffer = Vec::new();
            stdout.read_to_end(&mut buffer)?;
            Ok(buffer)
        })
        .transpose()
        .map_err(|err| JoinError::Io(OpaqueError::new(err)))?
        .unwrap_or_else(Vec::new);

    let stderr = handle
        .stderr
        .as_mut()
        .map(|stderr| -> std::io::Result<_> {
            let mut buffer = Vec::new();
            stderr.read_to_end(&mut buffer)?;
            Ok(buffer)
        })
        .transpose()
        .map_err(|err| JoinError::Io(OpaqueError::new(err)))?
        .unwrap_or_else(Vec::new);

    Ok((stdout, stderr))
}

fn get_return_value<T>(
    handle: &mut Child,
    return_receiver: &IpcBytesReceiver,
    decode_return: DecodeReturn<T>,
) -> Result<(T, ExitStatus), JoinError> {
    let status = handle
        .wait()
        .map_err(|err| JoinError::WaitFailed(OpaqueError::new(err)))?;

    let result = return_receiver.recv().map_err(|err| match err {
        IpcError::Bincode(_) => unreachable!("BytesReceiver should never use Bincode"),
        IpcError::Io(err) => JoinError::ReturnMissing(OpaqueError::new(err)),
        IpcError::Disconnected => {
            JoinError::ReturnMissing(OpaqueError::new("IPC channel disconnected"))
        }
    })?;

    let result: Result<Vec<u8>, ChildError> = rmp_serde::from_slice(&result).unwrap();

    let result = (decode_return)(result?).map_err(JoinError::ReturnMissing)?;

    Ok((result, status))
}
