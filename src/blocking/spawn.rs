use std::{
    io::{BufRead, BufReader, Read, Write},
    process::Stdio,
    sync::atomic::Ordering,
};

use rand::Rng;

use crate::{
    builder::{BufferMode, InputMode, OutputMode},
    callable::CallableOnce,
    callable_id::CallableHandle,
    data::{child::ChildData, parent::ParentData},
    errors::{OpaqueError, SpawnError},
    send_process::SendToProcess,
    Builder, SPORK_FAILED, SPORK_METHOD_CALLED,
};

use super::join_handle::JoinHandle;

pub struct StdSpawnProcess;

impl Builder<StdSpawnProcess> {
    pub fn new() -> Self {
        Builder::new_with_spawn_environment(StdSpawnProcess)
    }
}

impl Builder<StdSpawnProcess> {
    pub fn spawn<T, A, R>(&mut self, callable: T, args: A) -> Result<JoinHandle<R>, SpawnError>
    where
        T: CallableOnce<A, R> + 'static,
        A: SendToProcess,
        R: SendToProcess,
    {
        self.spawn_handle(CallableHandle::of(&callable), args)
    }

    pub fn spawn_handle<A, R>(
        &mut self,
        callable_handle: CallableHandle<A, R>,
        args: A,
    ) -> Result<JoinHandle<R>, SpawnError>
    where
        A: SendToProcess,
        R: SendToProcess,
    {
        if !SPORK_METHOD_CALLED.load(Ordering::Relaxed) {
            return Err(SpawnError::SporkNeverCalled);
        }

        if SPORK_FAILED.load(Ordering::Relaxed) {
            return Err(SpawnError::SporkFailed);
        }

        let (server, server_name) = ipc_channel::ipc::IpcOneShotServer::new()
            .map_err(|err| SpawnError::Io(OpaqueError::new(err)))?;

        let mut buffer = [0u8; 30];
        rand::thread_rng().fill(&mut buffer);
        let file_name = base64::encode(buffer).replace(['=', '/', '+'], "");
        let mut signal_file_path = std::env::temp_dir();
        signal_file_path.push(file_name);

        let data = ChildData {
            server_name,
            entry_id: callable_handle.id().encode(),
            signal_file_path: signal_file_path.clone(),
        };

        let self_path =
            std::env::current_exe().map_err(|err| SpawnError::Io(OpaqueError::new(err)))?;
        let mut command = std::process::Command::new(self_path);
        command
            .env_clear()
            .envs(&mut self.envs)
            .env(
                crate::ENV_VAR,
                base64::encode(rmp_serde::to_vec(&data).unwrap()),
            )
            .args(&mut self.args);

        let stdout_mode = match self.stdout.take(OutputMode::Disconnected) {
            mode @ OutputMode::Inherit => {
                command.stdout(Stdio::inherit());
                Some(mode)
            }
            mode @ OutputMode::Disconnected => {
                command.stdout(Stdio::null());
                Some(mode)
            }
            OutputMode::Custom(stdio) => {
                command.stdout(stdio);
                None
            }
            mode @ OutputMode::Buffered { .. } => {
                command.stdout(Stdio::piped());
                Some(mode)
            }
            mode @ OutputMode::Saved => {
                command.stdout(Stdio::piped());
                Some(mode)
            }
        };

        let stderr_mode = match self.stderr.take(OutputMode::Disconnected) {
            mode @ OutputMode::Inherit => {
                command.stderr(Stdio::inherit());
                Some(mode)
            }
            mode @ OutputMode::Disconnected => {
                command.stderr(Stdio::null());
                Some(mode)
            }
            OutputMode::Custom(stdio) => {
                command.stderr(stdio);
                None
            }
            mode @ OutputMode::Buffered { .. } => {
                command.stderr(Stdio::piped());
                Some(mode)
            }
            mode @ OutputMode::Saved => {
                command.stderr(Stdio::piped());
                Some(mode)
            }
        };

        let stdin_mode = match self.stdin.take(InputMode::Disconnected) {
            mode @ InputMode::Inherit => {
                command.stdin(Stdio::inherit());
                Some(mode)
            }
            mode @ InputMode::Disconnected => {
                command.stdin(Stdio::null());
                Some(mode)
            }
            InputMode::Custom(stdio) => {
                command.stdin(stdio);
                None
            }
            mode @ InputMode::Buffer(_) => {
                command.stdin(Stdio::piped());
                Some(mode)
            }
        };

        let mut handle = command
            .spawn()
            .map_err(|err| SpawnError::Io(OpaqueError::new(err)))?;

        let stdout_handle = run_output_handler(&mut handle.stdout, stdout_mode);

        let stderr_handle = run_output_handler(&mut handle.stderr, stderr_mode);

        match stdin_mode {
            Some(InputMode::Buffer(buffer)) => {
                let mut buffer = std::io::Cursor::new(buffer);
                if let Some(mut stdin) = handle.stdin.take() {
                    std::io::copy(&mut buffer, &mut stdin).unwrap();
                    stdin.flush().unwrap();
                    drop(stdin);
                } else {
                    panic!("bad")
                }
            }
            _ => {}
        }

        // Wait for child to either stop, signal file to be made, or timeout.
        loop {
            // Check for signal file.
            if signal_file_path.exists() {
                // Good to attempt to accept connection with child.
                break;
            }

            // Check if child process is still alive.
            match handle.try_wait() {
                Err(err) => {
                    handle
                        .kill()
                        .map_err(|err| SpawnError::Io(OpaqueError::new(err)))?;
                    return Err(SpawnError::Io(OpaqueError::new(err)));
                }
                Ok(Some(_)) => {
                    // dead
                    return Err(SpawnError::FailedToSendArgs(OpaqueError::new(
                        "child process did not connect to parent process",
                    )));
                }
                Ok(None) => {
                    // still alive
                }
            }

            // Yield to not take up all of the CPU's time.
            std::thread::yield_now();
        }

        let (_, data): (_, ParentData) = server
            .accept()
            .map_err(|err| SpawnError::Io(OpaqueError::new(err)))?;

        std::fs::remove_file(signal_file_path).unwrap();

        data.sender
            .send(&A::encode(&args).map_err(|err| SpawnError::ArgsEncode(err))?)
            .unwrap();

        Ok(JoinHandle {
            handle,
            decode_return: R::decode,
            return_receiver: data.receiver,
            stdout_thread_handle: stdout_handle,
            stderr_thread_handle: stderr_handle,
            kill_on_drop: self.kill_on_drop,
        })
    }
}

fn run_output_handler<T>(
    stdio: &mut Option<T>,
    output_mode: Option<OutputMode>,
) -> Option<std::thread::JoinHandle<()>>
where
    T: Read + Send + 'static,
{
    let stdio_mode = output_mode?;

    let (mode, mut device) = if let OutputMode::Buffered { mode, device } = stdio_mode {
        (mode, device)
    } else {
        return None;
    };

    // Shouldn't be possible to be None here.
    let mut stdio = stdio.take()?;

    let handler = move || match mode {
        Some(BufferMode::Line) => {
            let mut buffered = BufReader::new(stdio);
            let mut line = String::new();
            loop {
                if buffered.read_line(&mut line).unwrap() == 0 {
                    break;
                }
                device.write_all(line.as_bytes()).unwrap();
                device.flush().unwrap();
                line.clear();
            }
        }
        Some(BufferMode::Custom(amount)) => {
            let mut buffer = vec![0u8; amount];
            loop {
                let length = stdio.read(&mut buffer).unwrap();
                if length == 0 {
                    break;
                }
                device.write_all(&buffer[..length]).unwrap();
                device.flush().unwrap();
            }
        }
        Some(BufferMode::Full) => {
            let mut buffer = Vec::new();
            let mut read_buffer = [0; 1024];
            loop {
                let length = stdio.read(&mut read_buffer).unwrap();
                if length == 0 {
                    break;
                }
                buffer.extend(&read_buffer[..length]);
            }
            device.write_all(&buffer).unwrap();
            device.flush().unwrap();
        }
        None => {
            let mut read_buffer = [0; 1024];
            loop {
                let length = stdio.read(&mut read_buffer).unwrap();
                if length == 0 {
                    break;
                }
                device.write_all(&read_buffer[..length]).unwrap();
            }
            device.flush().unwrap();
        }
    };

    Some(std::thread::spawn(handler))
}
