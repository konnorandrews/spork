use crate::callable::CallableOnce;
use crate::callable_id::CallableId;
use crate::callable_id::EncodedCallableID;
use crate::send_process::SendToProcess;
use std::collections::HashMap;
use std::marker::PhantomData;
use std::sync::atomic::Ordering;

use super::*;

pub trait DispatchEntry {
    fn run(self: Box<Self>, encoded_args: Vec<u8>) -> Result<Vec<u8>, ChildError>;
}

struct Entry<T, A, R> {
    target: T,
    marker: PhantomData<fn() -> (A, R)>,
}

pub struct DispatchTable {
    pub(super) table: HashMap<EncodedCallableID, Box<dyn DispatchEntry>>,
    before_exit_hook: Option<Box<dyn FnOnce(&Result<(), OpaqueError>)>>,
}

impl DispatchTable {
    pub fn new() -> Self {
        Self {
            table: HashMap::new(),
            before_exit_hook: None,
        }
    }
}

impl Default for DispatchTable {
    fn default() -> Self {
        Self::new()
    }
}

impl DispatchTable {
    pub fn insert<T, A, R>(&mut self, callable: T) -> &mut Self
    where
        T: CallableOnce<A, R> + 'static,
        A: SendToProcess,
        R: SendToProcess,
    {
        self.table.insert(
            CallableId::of(&callable).encode(),
            Box::new(Entry {
                target: callable,
                marker: PhantomData,
            }),
        );

        self
    }

    /// Check if the callable requested by the parent process has been registered on this
    /// `DispatchTable`.
    ///
    /// If called in the main process, then `None` is returned. Otherwise, `true` is returned
    /// if the parent process requested a callable this `DispatchTable` can run. `false` is
    /// returned if the callable has not been registered to this `DispatchTable` instance.
    ///
    /// This is useful if multiple `DispatchTable` instances are being used.
    pub fn is_requested_callable_registered(&self) -> Option<Result<bool, OpaqueError>> {
        // If the process is the main one then there isn't a requested callable.
        if let ProcessType::Main = current_process_type() {
            return None;
        }

        Some((|| {
            // Unwrap is used because is_child already checked for it.
            // If it's not utf-8 then something bad happend.
            let data = std::env::var(crate::ENV_VAR).unwrap();
            let data = base64::decode(data).map_err(OpaqueError::new)?;
            let data: ChildData = rmp_serde::from_slice(&data).map_err(OpaqueError::new)?;

            // Check if the requested entry has been registered with this DispatchTable.
            Ok(self.table.contains_key(&data.entry_id))
        })())
    }

    /// Unset the internal flag that `spork` or `try_spork` has been called.
    ///
    /// Shouldn't really be needed. Its only useful for special usecases.
    pub fn unset_spork_called(_: &Self) {
        SPORK_METHOD_CALLED.store(false, Ordering::Relaxed);
    }
}

impl<T, A, R> DispatchEntry for Entry<T, A, R>
where
    T: CallableOnce<A, R>,
    A: SendToProcess,
    R: SendToProcess,
{
    fn run(self: Box<Self>, encoded_args: Vec<u8>) -> Result<Vec<u8>, ChildError> {
        // Try and decode the args.
        // If everything went well to here then this should always work.
        let args = A::decode(encoded_args).map_err(ChildError::ArgsDecode)?;

        // Run the callable.
        let result = CallableOnce::call(self.target, args);

        // Encode the return to be sent back to the parent.
        R::encode(&result).map_err(ChildError::ReturnEncode)
    }
}
