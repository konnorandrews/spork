use crate::callable::CallableOnce;
use crate::data::child::ChildData;
use crate::errors::OpaqueError;
use crate::process_type::{current_process_type, ProcessType};
use std::{
    any::{type_name, TypeId},
    hash::{Hash, Hasher},
    marker::PhantomData,
};

pub(crate) type EncodedCallableID = Vec<u8>;

/// ID of a callable type.
///
/// Both the `TypeId` and diagnostic type name are used.
#[derive(Clone, Copy)]
pub(crate) struct CallableId {
    id: TypeId,
    name: &'static str,
}

impl CallableId {
    /// Encode the contained `TypeId` and type name into an opaque byte sequence.
    ///
    /// This should always be stable over the same process source because no sources of randomness
    /// are used in it's calculation.
    ///
    /// The stability of the returned value between different builds of the same Rust source is not
    /// specified. This is a result of the stability `core` gives for `TypeId` and `type_name`.
    pub fn encode(&self) -> EncodedCallableID {
        // Get the raw bytes from the TypeId without unsafe.
        // This should always work given a reasonable implementation of Hash in core for TypeId.
        let mut h = MockHasher { data: Vec::new() };
        self.id.hash(&mut h);
        let mut data = h.data;

        // Calculate short stable hash of the type name using the SDBM hash algorithm.
        // This is included to reduce TypeId collision possibility even more.
        let mut h = SdbmHasher { state: 0 };
        self.name.hash(&mut h);
        data.extend(h.finish().to_be_bytes());

        data
    }
}

impl std::fmt::Debug for CallableId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        // Only show the type name.
        f.debug_tuple("CallableId").field(&self.name).finish()
    }
}

impl CallableId {
    /// Get ID of a given callable.
    pub fn of<T, A, R>(_: &T) -> Self
    where
        T: CallableOnce<A, R> + ?Sized + 'static,
    {
        Self {
            id: TypeId::of::<T>(),
            name: type_name::<T>(),
        }
    }
}

/// Handle to a callable.
///
/// This type can be used to reference a callable that requires ownership to store.
/// And example of such a callable is `move |b| a + b` where `a` is not `Copy`.
/// Handles can also be made to all callable types for convenience.
///
/// The signature of the callable used to make the handle is stored in the `A` and `R` generic
/// parameters, and is used for type checking generic calls.
pub struct CallableHandle<A, R> {
    id: CallableId,
    marker: PhantomData<fn(A) -> R>,
}

impl<A, R> Copy for CallableHandle<A, R> {}

impl<A, R> Clone for CallableHandle<A, R> {
    fn clone(&self) -> Self {
        *self
    }
}

impl<A, R> CallableHandle<A, R> {
    /// Get handle to a given callable.
    ///
    /// This function takes a borrow of the callable to automatically infer `T`.
    /// This is required for un-nameable types like function items and closures.
    /// The resulting handle can be used as a name for those un-nameable types.
    ///
    /// Non-`'static` callable types are not currently allowed.
    pub fn of<T>(callable: &T) -> Self
    where
        T: CallableOnce<A, R> + ?Sized + 'static,
    {
        Self {
            id: CallableId::of(callable),
            marker: PhantomData,
        }
    }

    /// Check if the callable requested by the parent process is the callable this handle
    /// represents.
    ///
    /// If called in the main process, then `None` is returned.
    pub fn is_requested(&self) -> Option<Result<bool, OpaqueError>> {
        // If the process is the main one then there isn't a requested callable.
        if let ProcessType::Main = current_process_type() {
            return None;
        }

        Some((|| {
            // Unwrap is used because is_child already checked for it.
            // If it's not utf-8 then something bad happend.
            let data = std::env::var(crate::ENV_VAR).unwrap();
            let data = base64::decode(data).map_err(OpaqueError::new)?;
            let data: ChildData = rmp_serde::from_slice(&data).map_err(OpaqueError::new)?;

            // Check if the requested callable is the same has this handle.
            Ok(self.id().encode() == data.entry_id)
        })())
    }

    /// Get the underlying `CallableId`.
    pub(crate) fn id(&self) -> CallableId {
        self.id
    }
}

impl<A, R> std::fmt::Debug for CallableHandle<A, R> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        // Show the ID and the signature.
        f.debug_struct("CallableHandle")
            .field("id", &self.id)
            .field(
                "signature",
                &format!(
                    "fn({}) -> {}",
                    type_name::<A>()
                        .strip_prefix('(')
                        .unwrap_or("")
                        .strip_suffix(')')
                        .unwrap_or("")
                        .strip_suffix(',')
                        .unwrap_or(""),
                    type_name::<R>()
                ),
            )
            .finish()
    }
}

impl<A, R> PartialEq for CallableHandle<A, R> {
    fn eq(&self, other: &Self) -> bool {
        // Equality is based on the encoded form of the ID.
        self.id.encode() == other.id.encode()
    }
}

impl<A, R> Eq for CallableHandle<A, R> {}

impl<A, R> Hash for CallableHandle<A, R> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.id.encode().hash(state);
    }
}

/// Hasher for getting the internal data of `TypeId`.
struct MockHasher {
    data: Vec<u8>,
}

impl Hasher for MockHasher {
    fn finish(&self) -> u64 {
        unimplemented!();
    }

    fn write(&mut self, bytes: &[u8]) {
        self.data.extend(bytes);
    }
}

/// Simple stable hasher for type names.
struct SdbmHasher {
    state: u64,
}

impl Hasher for SdbmHasher {
    fn finish(&self) -> u64 {
        self.state
    }

    fn write(&mut self, bytes: &[u8]) {
        for &byte in bytes {
            self.state = (byte as u64)
                .wrapping_add(self.state << 6)
                .wrapping_add(self.state << 16)
                .wrapping_sub(self.state);
        }
    }
}

#[test]
fn callable_handle() {
    let a = CallableHandle::of(&|_x: f32| 0);
    let b = CallableHandle::of(&|_x: f32| 0);

    // dbg!(a);
    // dbg!(b);
    // dbg!(a.id().encode());
    // dbg!(b.id().encode());

    assert!(a != b);
    assert!(a == a);
    assert!(b == b);
}
